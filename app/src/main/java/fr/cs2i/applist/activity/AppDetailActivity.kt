package fr.cs2i.applist.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import fr.cs2i.applist.R
import fr.cs2i.applist.model.Application

class AppDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_detail)

        // recuperer l'application passée dans l'intent
        val app = intent.getSerializableExtra("selectedApp") as Application

        // recuperer toutes les vues
        val logoView: ImageView = findViewById(R.id.logo)
        val titleView: TextView = findViewById(R.id.nomJeu)
        val editorView: TextView = findViewById(R.id.editeur)
        val ratingView: TextView = findViewById(R.id.note)
        val downloadsView: TextView = findViewById(R.id.nbDL)
        val descriptionView: TextView = findViewById(R.id.description)

        // transferer l'application dans les vues
        logoView.setImageResource(app.icone)
        titleView.text = app.nomApplication
        editorView.text = app.auteur
        ratingView.text = app.note.toString()
        downloadsView.text = app.telechargements.toString()
        descriptionView.text = app.description


    }
}