package fr.cs2i.applist.activity

import android.content.Intent
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.cs2i.applist.R
import fr.cs2i.applist.adapter.ApplicationAdapter
import fr.cs2i.applist.model.Application

class MainActivity : AppCompatActivity() {

    enum class DisplayMode {
        List,
        Grid
    }

    var displayMode = false
    var displayMode2 = ""
    lateinit var fab: FloatingActionButton

    ///////////////////////////////////////////////////////////////////////////
    // CYCLE DE VIE
    ///////////////////////////////////////////////////////////////////////////

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(findViewById(R.id.toolbar))

        // definition d'un fab
        fab = findViewById<FloatingActionButton>(R.id.fab)

        fab.setOnClickListener { view ->

            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
        }

        // afficher la liste des applications
        displayApps()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }


    ///////////////////////////////////////////////////////////////////////////
    // GESTION DES APPLICATIONS
    ///////////////////////////////////////////////////////////////////////////

    private fun displayApps() {
        // liste des applis
        val apps = lireApplications()

        // La RecyclerView
        val appRecyclerView: RecyclerView = findViewById(R.id.listApp)

        // choisir un LayoutManager List ou Grid
        if (displayMode == false) {
            // l'adapter
            val adapter = ApplicationAdapter(
                this,
                R.layout.item_application,
                apps
            )

            // "plaquer" l'adaptateur sur la RecyclerView
            appRecyclerView.adapter = adapter
            appRecyclerView.layoutManager = LinearLayoutManager(this)
            // changement de l'image du fab
            fab.setImageResource(R.drawable.listicon)

        } else {
            // l'adapter
            val adapter = ApplicationAdapter(
                this,
                R.layout.item_application_grid,
                apps
            )

            // "plaquer" l'adaptateur sur la RecyclerView
            appRecyclerView.adapter = adapter
            appRecyclerView.layoutManager = GridLayoutManager(this, 2)
            // changement de l'image du fab
            fab.setImageResource(R.drawable.gridicon)
        }

    }

    /**
     * Liste des applications
     */
    fun lireApplications(): List<Application> {
        val applications = mutableListOf<Application>()

        // code
        for (i in 1..10) {
            applications.add(
                Application(
                    "Clash Royale",
                    "SUPERCELL",
                    4.6,
                    100000000,
                    R.drawable.clash_royale,
                    "Entrez dans l'arène ! Les créateurs de Clash of Clans vous offrent un jeu multijoueur en temps réel mettant en scène des combattants royaux, vos personnages préférés de Clash of Clans et beaucoup plus encore."
                )
            )

            applications.add(
                Application(
                    "King of Avalon: Dragon Warfare",
                    "DIANDIAN INTERACTIVE HOLDING",
                    4.2,
                    10000000,
                    R.drawable.dragon_warfare,
                    "Fais toi des alliés et combats des ennemis du monde entier, ton dragon et le trône t’attendent !"
                )
            )

            applications.add(
                Application(
                    "Idle Heroes",
                    "DHGAMES",
                    4.7,
                    10000000,
                    R.drawable.idle_heroes,
                    "Rejoignez des millions de joueurs de toute la planète et commencez votre journée de la Foret de Sara jusqu'au Paradis Volant et menez vos héros aux ancienne ruines pour vaincre les forces du mal !"
                )
            )

            applications.add(
                Application(
                    "Suzy Cube",
                    "NOODLECAKE STUDIOS INC",
                    4.3,
                    1000,
                    R.drawable.suzy_cube,
                    "Oh non ! Ces vilains Nonos ont volé tout l'or caché au château de Cubeton ! Seule Suzy Cube peut récupérer les trésors du château volés par ces vilains pas beaux !"
                )
            )

            applications.add(
                Application(
                    "Bacterial Takeover-Idle Cliker",
                    "SIA FUFLA",
                    4.5,
                    1000000,
                    R.drawable.bacterial_takeover,
                    "Bienvenue, docteur ! Nous vous avons attendu. Tout est préparé pour que vous preniez le relais et que vous commenciez à cultiver des bactéries pour initier une prise de conscience bactérienne !"
                )
            )
        }


        return applications
    }
}