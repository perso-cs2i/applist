package fr.cs2i.applist.model

import fr.cs2i.applist.R
import java.io.Serializable

class Application(
    var nomApplication: String,
    var auteur: String,
    var note: Double,
    var telechargements: Int,
    var icone: Int,
    var description: String=""
): Serializable

{


}