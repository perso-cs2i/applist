package fr.cs2i.applist.adapter

import android.content.Context
import android.content.Intent
import android.text.Layout
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import fr.cs2i.applist.R
import fr.cs2i.applist.activity.AppDetailActivity
import fr.cs2i.applist.model.Application
import kotlinx.android.synthetic.main.item_application.view.*

class ApplicationAdapter(
    private val context: Context,
    private val itemLayout: Int,
    private val applications: List<Application>
): RecyclerView.Adapter<ApplicationAdapter.ViewHolder>() {

    // ---
    // VIEWHOLDER
    // ---
    // Gestionnaire de vues
    // * Buffer* de vues
    // On crée autant d'objets Kotlin (TextView, ImageViewe, ...)
    // qu'on a d'objets XML dans le "item layout"
    class ViewHolder(itemView: View):
            RecyclerView.ViewHolder(itemView) {

        val containerApps = itemView
        //val containerApps: ConstraintLayout = itemView as ConstraintLayout
        val logoView: ImageView = itemView.findViewById(R.id.logo)
        val titleView: TextView = itemView.findViewById(R.id.nomJeu)
        val editorView: TextView = itemView.findViewById(R.id.editeur)
        val ratingView: TextView = itemView.findViewById(R.id.note)
        val downloadsView: TextView = itemView.findViewById(R.id.nbDL)

        val descriptionView: TextView = itemView.findViewById(R.id.description)
        val detailButton: Button = itemView.findViewById(R.id.detail)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater
            .from(context)
            .inflate(itemLayout, parent, false)

        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return applications.size
    }


    // dispatcher les infos d'une seule application
    // dans les différentes vues enfants de l'item layout

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // recupérer l'application demandée par RecyclerView
        val application = applications[position]

        // dispatcher l'application dans les vues enfants
        holder.titleView.text = application.nomApplication
        holder.editorView.text = application.auteur
        holder.downloadsView.text = application.telechargements.toString()
        holder.ratingView.text = application.note.toString()
        holder.logoView.setImageResource(application.icone)
        
        // on est entrain de lier une ouvelle application à une vue qui peut être recyclée
        // -> par sécurité, on masque la description et le bouton
        holder.descriptionView.visibility = View.GONE
        holder.detailButton.visibility = View.GONE

        holder.containerApps.setOnClickListener {
            if(holder.descriptionView.visibility == View.VISIBLE)
            {
                holder.descriptionView.visibility = View.GONE
                holder.detailButton.visibility = View.GONE
            } else {
                holder.descriptionView.visibility = View.VISIBLE
                holder.detailButton.visibility = View.VISIBLE
            }
        }

        holder.detailButton.setOnClickListener {
            val intent = Intent(context, AppDetailActivity::class.java)
            intent.putExtra("selectedApp", application)
            context.startActivity(intent)
        }
    }
}